/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_' + process.env.NODE_ENV)
// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/** note: submenu only apppear when children.length>=1
*   detail see  https://panjiachen.github.io/vue-element-admin-site/#/router-and-nav?id=sidebar
**/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if fasle ,the page will no be cached(default is false)
  }
**/
export const constantRouterMap = [
  { path: '/api', component: _import('APItest/api'), hidden: true },
  { path: '/login', component: _import('login/index'), hidden: true },
  { path: '/authredirect', component: _import('login/authredirect'), hidden: true },
  { path: '*', component: _import('errorPage/404'), hidden: true },
  { path: '/401', component: _import('errorPage/401'), hidden: true },
  {
    path: '',
    component: Layout,
    redirect: 'gailan/overView',
    children: [{
      path: 'gailan/overView',
      component: _import('device/overView/index'),
      name: '概览',
      meta: { title: 'gailan', icon: 'dashboard', noCache: true }
    }]
  }
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [
  {
    path: '/gailan',
    component: Layout,
    name: '概览',
    icon: 'setting',
    authority: 'gailan',
    children: [
      {
        path: 'overView',
        icon: 'fa-user',
        component: _import('device/overView/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      }
    ]
  },
  {
    path: '/dianqihuozai',
    component: Layout,
    name: '电气火灾',
    icon: 'setting',
    authority: 'dianqihuozai',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/fireDeviceManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      }
    ]
  },
  {
    path: '/huozaibaojing',
    component: Layout,
    name: '火灾报警',
    icon: 'setting',
    authority: 'huozaibaojing',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/fireDeviceManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      },
    ]
  },
  {
    path: '/hydrant',
    component: Layout,
    name: '消防给水',
    icon: 'setting',
    authority: 'hydrant',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/hydrantindex'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/outdoorRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/outdoorAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/hydrantManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      },
      {
        path: 'hardwareSet/:id',
        icon: 'fa-user',
        component: _import('device/hardwareSet/index'),
        name: '硬件实施',
        authority: 'hardwareSet'
      }
    ]
  },
  {
    path: '/gasFirecontrol',
    component: Layout,
    name: '气体灭火',
    icon: 'setting',
    authority: 'gasFirecontrol',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/fireDeviceManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      },
    ]
  },
  {
    path: '/combustible',
    component: Layout,
    name: '可燃气体',
    icon: 'setting',
    authority: 'combustible',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/fireDeviceManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      },
    ]
  },
  {
    path: '/firedoor',
    component: Layout,
    name: '防火门',
    icon: 'setting',
    authority: 'firedoor',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/fireDeviceManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      },
    ]
  },
  {
    path: '/evacuation',
    component: Layout,
    name: '应急疏散',
    icon: 'setting',
    authority: 'evacuation',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/realtimeMonit/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/index'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/index'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/fireDeviceManger/index'),
        name: '设备管理',
        authority: 'fireDeviceManger'
      },
    ]
  },
  {
    path: '/videoSurveillance',
    component: Layout,
    name: '危险预警',
    icon: 'setting',
    authority: 'videoSurveillance',
    children: [
      {
        path: 'videoWatch/:id',
        icon: 'fa-user',
        component: _import('device/videoWatch/index'),
        name: '实时监测',
        authority: 'videoWatch'
      },
      {
        path: 'videoAbnormal/:id',
        icon: 'fa-user',
        component: _import('device/videoAbnormal/index'),
        name: '异常记录',
        authority: 'videoAbnormal'
      },
      {
        path: 'videoAnalyze/:id',
        icon: 'fa-user',
        component: _import('device/videoAnalyze/index'),
        name: '统计分析',
        authority: 'videoAnalyze'
      },
      {
        path: 'videoDeviceManger/:id',
        icon: 'fa-user',
        component: _import('device/videoDeviceManger/index'),
        name: '设备管理',
        authority: 'videoDeviceManger'
      },
      {
        path: 'identifyDevice',
        icon: 'fa-user',
        component: _import('device/videoAnalyze/components/identifyDevice'),
        name: '识别设备',
        authority: 'videoSurveillance'
      }
    ]
  },
  {
    path: '/fireMainframe',
    component: Layout,
    name: '消防主机',
    icon: 'setting',
    authority: 'fireMainframe',
    children: [
      {
        path: 'realtimeMonit/:id',
        icon: 'fa-user',
        component: _import('device/fireMainframe/index'),
        name: '实时监测',
        authority: 'realtimeMonit'
      },
      {
        path: 'exceptionRecord/:id',
        icon: 'fa-user',
        component: _import('device/exceptionRecord/fireMindex'),
        name: '异常记录',
        authority: 'exceptionRecord'
      },
      {
        path: 'statisticAnly/:id',
        icon: 'fa-user',
        component: _import('device/statisticAnly/fireMindex'),
        name: '统计分析',
        authority: 'statisticAnly'
      },
      {
        path: 'fireFrameManger/:id',
        icon: 'fa-user',
        component: _import('device/fireMainframe/mangerindex'),
        name: '主机管理',
        authority: 'fireFrameManger'
      },
      {
        path: 'fireRelation/:id',
        icon: 'fa-user',
        component: _import('device/fireMainframe/components/realtionDevice'),
        name: '主机关联设备',
        authority: 'fireMainframe'
      },
    ]
  },
  {
    path: '/inspectionManagement',
    component: Layout,
    name: '巡检管理',
    icon: 'setting',
    authority: 'inspectionManagement',
    children: [
      {
        path: 'facilitiesType/:id',
        icon: 'fa-user',
        component: _import('device/facilityTypeManger/cindex'),
        name: '设施类型',
        authority: 'facilitiesType'
      },
      {
        path: 'sbxj/:id',
        icon: 'fa-user',
        component: _import('device/sbxj/sbxj'),
        name: '设施巡检',
        authority: 'sbxj'
      },
      {
        path: 'facilityManager/:id',
        icon: 'fa-user',
        component: _import('device/facilityManager/index'),
        name: '设施管理',
        authority: 'facilityManager'
      },
      {
        path: 'inspectionRoute/:id',
        icon: 'fa-user',
        component: _import('device/inspectionManager/inspectroute'),
        name: '巡检路线',
        authority: 'inspectionRoute'
      },
      {
        path: 'patrolSchemeManager/:id',
        icon: 'fa-user',
        component: _import('device/patrolScheme/index'),
        name: '巡检计划',
        authority: 'patrolSchemeManager'
      },
      {
        path: 'inspectionRecord/:id',
        icon: 'fa-user',
        component: _import('device/inspectionRecord/index'),
        name: '巡检记录',
        authority: 'inspectionRecord'
      }
    ]
  },
  {
    path: '/adminSys',
    component: Layout,
    name: '基础配置管理',
    icon: 'setting',
    authority: 'adminSys',
    children: [
      {
        path: 'stationConfig',
        icon: 'fa-user',
        component: _import('device/deviceMeasuringPoint/index'),
        name: '测点配置',
        authority: 'stationConfig'
      },
      {
        path: 'deviceManger',
        icon: 'fa-user',
        component: _import('device/deviceManger/index'),
        name: '设备管理',
        authority: 'deviceManger'
      },
      {
        path: 'deviceManger/:id',
        icon: 'fa-user',
        component: _import('device/deviceManger/index'),
        name: '设备管理',
        authority: 'deviceManger'
      },
      {
        path: 'sysConfig',
        icon: 'fa-user',
        component: _import('device/devicesysConfig/index'),
        name: '系统设置',
        authority: 'sysConfig',
      },
      {
        path: 'manageRules/:id',
        icon: 'fa-user',
        component: _import('device/devicesysConfig/components/manageRules'),
        name: '管理规则',
        authority: 'adminSys'
      },
      {
        path: 'alarmLevel',
        icon: 'fa-user',
        component: _import('device/deviceAlarmLevel/index'),
        name: '报警等级',
        authority: 'alarmLevel'
      },
      {
        path: 'deviceSeries',
        icon: 'fa-user',
        component: _import('device/deviceSensorSeries/index'),
        name: '设备系列',
        authority: 'deviceSeries'
      },
      {
        path: 'userManager',
        icon: 'fa-user',
        component: _import('admin/user/index'),
        name: '用户管理',
        authority: 'userManager'
      }, {
        path: 'menuManager',
        icon: 'category',
        component: _import('admin/menu/index'),
        name: '菜单管理',
        authority: 'menuManager'
      }, {
        path: 'wlz-ceshi1',
        icon: 'category',
        component: _import('admin/ceshi/ceshi'),
        name: '测试',
        authority: 'menuManager'
      },
      {
        path: 'elklog',
        icon: 'category',
        component: _import('device/elklog/index'),
        name: 'elk日志',
        authority: 'elklog'
      },

      {
        path: 'workflow',
        icon: 'category',
        component: _import('device/workflow/index'),
        name: '流程预案',
        authority: 'workflow'
      },
      {
        path: 'dataconn',
        icon: 'category',
        component: _import('device/dataconn/index'),
        name: '数据采集',
        authority: 'dataconn'
      },
      {
        path: 'facilityTypeManger',
        icon: 'category',
        component: _import('device/facilityTypeManger/index'),
        name: '设施类型管理',
        authority: 'facilityTypeManger'
      },
      {
        path: 'groupManager',
        icon: 'group_fill',
        component: _import('admin/group/index'),
        name: '角色权限管理',
        authority: 'groupManager'
      }, {
        path: 'gateLogManager',
        icon: 'viewlist',
        component: _import('admin/gateLog/index'),
        name: '登录日志管理',
        authority: 'gateLogManager'
      }, {
        path: 'tenantManager',
        icon: 'tenant',
        component: _import('admin/tenant/index'),
        name: '租户管理',
        authority: 'tenantManager'
      }, {
        path: 'buildManager',
        icon: 'build',
        component: _import('admin/build/index'),
        name: '建筑管理',
        authority: 'buildManager'
      }, {
        path: 'networkManager',
        icon: 'network',
        component: _import('admin/network/index'),
        name: '联网单位管理',
        authority: 'networkManager'
      }, {
        path: 'firmManager',
        icon: 'firm',
        component: _import('admin/firm/index'),
        name: '厂商管理',
        authority: 'firmManager'
      }]
  }
]
