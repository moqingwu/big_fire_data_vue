/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import Cookies from 'js-cookie'
import { setItem, getItem, removeItem } from '@/utils/nocookie'
const TokenKey = 'Authorization'

export function getToken() {
  return getItem(TokenKey)!==undefined?getItem(TokenKey):''
}

export function setToken(token) {
  return setItem(TokenKey, token)
}

export function removeToken() {
  return removeItem(TokenKey)
}

