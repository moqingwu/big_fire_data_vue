/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2019/4/8
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */
import request from '@/utils/request'
//列表
export function getlistUserPage(query) {
  return request({
    url: '/api/datahandler/noticeRule/listNoticeRuleForUser',
    method: 'get',
    params: query
  })
}

//改变状态
export function changeUserStatus(obj) {
   return request({
     url:'/api/datahandler/noticeRuleUser/configNoticeRuleForUser',
     method: 'post',
     data: obj
   })
}
