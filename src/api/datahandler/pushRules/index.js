/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2019/4/9
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'
//列表
export function page(query) {
  return request({
    url: '/api/datahandler/noticeRule/listNoticeRule',
    method: 'get',
    params: query
  })
}

//列表删除前的查询
export function delQuery(query) {
  return request({
    url: '/api/datahandler/noticeRule/countBindUser',
    method: 'get',
    params: query
  })
}
//删除操作
export function delObj(query) {
  return request({
    url:'/api/datahandler/noticeRule/del',
    method: 'get',
    params: query
  })
}
//所属系统条件查询
export function affiliated(query) {
  return request({
    url: '/api/admin/channel/list',
    method: 'get',
    params: query
  })
}
//列表页添加规则
export function addObj(obj) {
  return request({
    url:'/api/datahandler/noticeRule',
    method: 'post',
    data: obj
  })
}
//消防主机传感器列表-已绑定
export function firemainPage(query) {
  return request({
    url: '/api/datahandler/noticeRuleSensor/listFireMainBindedSensor',
    method: 'get',
    params: query
  })
}
//删除规则绑定的传感器
export function deleteSensor(obj) {
  return request({
    url:'/api/datahandler/noticeRuleSensor/delBindedSensor',
    method: 'post',
    data: obj
  })
}
//添加传感器与推送规则的关联关系
export function addSensor(obj) {
  return request({
    url:'/api/datahandler/noticeRuleSensor/batchAdd',
    method: 'post',
    data: obj
  })
}
//消防主机未绑定推送规则的传感器列表
export function unFiremainPage(query) {
  return request({
    url: '/api/datahandler/noticeRuleSensor/listFireMainUnBindedSensor',
    method: 'get',
    params: query
  })
}

//非消防主机已绑定推送规则的传感器列表
export function devicePage(query) {
  return request({
    url: '/api/datahandler/noticeRuleSensor/listBindedSensor',
    method: 'get',
    params: query
  })
}
//查询推送规则未关联的传感器列表（适用于电气火灾、无线感烟、可燃气体、防火门等室内系统）
export function unDevicePage(query) {
  return request({
    url: '/api/datahandler/noticeRuleSensor/listUnBindedSensorForIndoorSystem',
    method: 'get',
    params: query
  })
}
//查询推送规则未关联的传感器列表（适用于危险预警等设备内嵌系统）
export function unfireDevicePage(query) {
  return request({
    url: '/api/datahandler/noticeRuleSensor/listUnBindedSensorForOutdoorSystem',
    method: 'get',
    params: query
  })
}

//查询推送规则未关联的传感器列表（适用于危险预警等设备内嵌系统）
export function unNestedPage(query) {
  return request({
    url: '/api/datahandler/noticeRuleSensor/listUnBindedSensorForNestedSystem',
    method: 'get',
    params: query
  })
}
