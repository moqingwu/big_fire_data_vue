/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

import { randomn } from '@/utils';


export function loginByUsername(username, password, code, image) {
  const data = {
    username,
    password: window.btoa(window.btoa(password) + randomn(7)),
    code,
    image,
    grant_type: 'password'
  }
  return request({
    url: '/api/auth/oauth/token',
    method: 'post',
    data,
    transformRequest: [function(data) {
      // Do whatever you want to transform the data
      let ret = ''
      for (const it in data) {
        ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
      }
      return ret
    }],
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export function logout(token) {
  return request({
    url: '/api/auth/oauth/token',
    method: 'delete',
    params: {
      access_token: token
    }
  })
}

export function getInfo() {
  return request({
    url: '/api/admin/user/front/info',
    method: 'get'
  })
}

export function getMenus() {
  return request({
    url: '/api/admin/user/front/menus',
    method: 'get'
  })
}

export function getAllMenus() {
  return request({
    url: '/api/admin/user/front/menu/all',
    method: 'get'
  })
}

export function isSuperAdmin(query) {
  return request({
    url: '/api/admin/user/isSuperAdmin',
    method: 'get',
    params: query
  })
}
