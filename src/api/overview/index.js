/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function getAllCount() {
  return request({
    url: '/api/device/deviceSensor/getAllCount',
    method: 'get',
  })
}
//pei图表
export function getPiechart() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getRatio',
    method: 'get',
  })
}

//报警图表
export function getalram() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getCallCount',
    method: 'get',
  })
}
//故障图表
export function getabnormal() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getFaultCount',
    method: 'get',
  })
}
//地图上的数据集合
export function getMapData() {
  return request({
    url: '/api/datahandler/deviceFacilitiesAbnormal/getCount',
    method: 'get',
  })
}

// 概览新增接口
export function getAllBuildingStatus() {
  return request({
    url: '/api/device/deviceBuilding/getAllBuildingStatus',
    method: 'get'
  })
}
