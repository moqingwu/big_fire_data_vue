/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function getWarningMessage(query) {
  return request({
    url: '/api/datahandler/deviceAbnormal/getNothandlerCount',
    method: 'get',
    params: query
  })
}
