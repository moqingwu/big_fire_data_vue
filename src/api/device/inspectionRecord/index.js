/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'
// tag列表页
export function page(query) {
  return request({
    url: '/api/device/deviceIndoorRecordInspectionResults/pageList',
    method: 'get',
    params: query
  })
}
//室内明细
export function details(query) {
  return request({
    url: '/api/device/deviceIndoorRecordInspectionResults/get',
    method: 'get',
    params: query
  })
}
//室内记录导出
export function exportRecord(query) {
  return request({
    url: '/api/device/deviceIndoorRecordInspectionResults/export',
    method: 'get',
    params: query
  })
}
//室内漏检记录
export function pageloss(query) {
  return request({
    url: '/api/device/deviceIndoorRecordInspectionResults/pageListLeak',
    method: 'get',
    params: query
  })
}
//室外
// tag列表页
export function outerpage(query) {
  return request({
    url: '/api/device/deviceOutdoorRecordInspectionResults/pageList',
    method: 'get',
    params: query
  })
}
//室外明细
export function outerdetails(query) {
  return request({
    url: '/api/device/deviceOutdoorRecordInspectionResults/get',
    method: 'get',
    params: query
  })
}
//室外记录导出
export function OuterexportRecord(query) {
  return request({
    url: '/api/device/deviceIndoorRecordInspectionResults/export',
    method: 'get',
    params: query
  })
}
//室外漏检记录
export function Outerpageloss(query) {
  return request({
    url: '/api/device/deviceOutdoorRecordInspectionResults/pageListLeak',
    method: 'get',
    params: query
  })
}
//室内设施类型联想搜
export function insiderLinkwith(query) {
  return request({
    url: '/api/device/deviceIndoorRecordInspectionResults/getSelected',
    method: 'get',
    params: query
  })
}
//室外设施类型联想搜
export function outerLinkwith(query) {
  return request({
    url: '/api/device/deviceOutdoorRecordInspectionResults/getSelected',
    method: 'get',
    params: query
  })
}
