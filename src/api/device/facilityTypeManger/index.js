/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function page(query) {
  //table
  return request({
    url: '/api/device/deviceFacilitiesType/pageList',
    method: 'get',
    params: query
  })
}

export function addObj(obj) {
  return request({
    url: '/api/device/deviceFacilitiesType/add',
    method: 'post',
    data: obj
  })
}

export function putObj(obj) {
  return request({
    url: '/api/device/deviceFacilitiesType/update',
    method: 'post',
    data: obj
  })
}

export function getObj(query) {
  return request({
    url: '/api/device/deviceFacilitiesType/get',
    method: 'get',
    params: query
  })
}

export function deleteQuery(query) {
  return request({
    url: '/api/device/deviceFacilitiesType/deleteQuery',
    method: 'get',
    params: query
  })
}

export function delObj(query) {
  return request({
    url: '/api/device/deviceFacilitiesType/delete',
    method: 'get',
    params: query
  })
}
