/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function getImgAuth() {
    return request({
	    url: '/api/device/deviceTenantSetup/get',
	    method: 'get'
    })
}

export function setImgAuth(obj) {
    return request({
	    url: '/api/device/deviceTenantSetup/addOrUpdate',
	    method: 'post',
	    data:obj
    })
}
