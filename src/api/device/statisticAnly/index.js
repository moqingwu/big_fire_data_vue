/*
 * Copyright (c) 2021.  北京维数科技有限公司
 * Date :  2021/3/25
 * Author ：F12 Console=> atob("MzMxNDkxODAwQHFxLmNvbQ==")
 * Version ：1.0.0
 */

import request from '@/utils/request'

export function selectCountByBuild(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/selectCountByBuildId',
    method: 'get',
    params: query
    })
}

export function CountByType(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/selectCountByType',
    method: 'get',
    params: query
    })
}

export function CountByMonth(query) {
    return request({
    url: '/api/datahandler/deviceAbnormal/selectCountByDate',
    method: 'get',
    params: query
    })
}
//隐患处理情况
export function getDefect(query) {
  return request({
    url: '/api/datahandler/deviceAbnormal/getRatioByDate',
    method: 'get',
    params: query
  })
}
